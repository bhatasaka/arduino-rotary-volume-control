# **Arduino Volume Control**

**Control a computer's volume with an ATMEGA-32u4**

Connecting a rotary encoder to a 32u4 allows clockwise and counter-clockwise movement to increase and decrease the volume of a connected computer. If the encoder is equiped with a button, the button will mute the volume.

## About
---
Utilizing an interrupt driven system, this file interprets the movement from a quadrature rotary encoder. Volume up and down messages are sent through USB from the device to the connected computer.

This code is based off of [code originally by Adafruit](https://learn.adafruit.com/trinket-usb-volume-knob) modified to work with a 32u4. That Adafruit page has a lot of good information for those planning on using this code. For USB interfacing, the [Arduino HID Project](https://github.com/NicoHood/HID/) is used and must be added as a library.

The action that occurs for movement/button press is detected can be changed to any USB function that the Arduino HID Project supports, or any code/function that is desired.

*This code has been tested **only** on an [Adafruit ItsyBitsy 32u4](https://learn.adafruit.com/introducting-itsy-bitsy-32u4/) with a [Bourns PEC11-4215F-S24 rotary encoder](https://www.adafruit.com/product/377) and Windows 10.*

## Hardware setup
---
This code is set up using the [Bourns PEC11-4215F-S24 rotary encoder](https://www.adafruit.com/product/377) and an [Adafruit ItsyBitsy 32u4](https://learn.adafruit.com/introducting-itsy-bitsy-32u4/).

Encoder Pin | ItsyBitsy Pin | ItsyBitsy Interrupt Pin | 32u4 Pin
----------- | ------------- | ----------------------- | --------
A           | 1             | 3                       | PD3
B           | 3             | 0                       | PD0
Button      | SCK           | 4                       | PB1/SCLK


When looking at three pin side of the encoder, A is on the far right, B is far left and the ground is the middle pin. On the other side, either pin can go to the button input while the other goes to ground.

## Programming
---
include "HID-Project.h" and include <avr/sleep.h> are both used. The ItsyBitsy board compiler provided by Adafruit was imported to the Arduino IDE and must be used for the ItsyBitsy. Programming was done all in the Arduino IDE.