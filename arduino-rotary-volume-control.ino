/*
   @file    arduino-rotary-volume-control.ino
   @author  Bryan Hatasaka

   This program uses a rotary controller as a volume controller for
   an Adafruit ItsyBitsy 32u4 5V @ 16MHz. The device is used as a
   USB HID device. Turning the rotary controller clockwise turns the
   volume up and counter-clockwise turns the volume down. Pushing the
   knob mutes the volume.

   The rotary controller used must be set up to pull its pins low and
   have a quadrature so direction can be determined. The code was
   developed with a Bourns PEC11 12mm Incremental Encoder with 24 positions.

   In order to send media controls through USB, the HID-Project library is used.
   This is found through the Arduino library manager and at:
   https://github.com/NicoHood/HID/

   Last Updated: July 21, 2018
   V1.0

*/
#include "HID-Project.h"
#include <avr/sleep.h>

#define enc_PORT_REG    PIND
#define button_PORT_REG PINB

// Following the encoder datasheet, there are two inputs, A and B
// and these correspond to the quadrature table
// (uint8_t is an 8-bit unsigned data type, essentially a char)
const uint8_t enc_pin_A = 3; //PD3 and interrupt pin 3 on ItsyBitsy
const uint8_t enc_pin_B = 0; //PD0 and interrupt pin 0 on ItsyBitsy
const uint8_t enc_pin_A_phys = 1; //encoder A pin - GPIO 1 on ItsyBitsy 
const uint8_t enc_pin_B_phys = 3; //encoder B pin - GPIO 3 on ItsyBitsy

const uint8_t enc_button_pin = 1; // PB1 - SCK
const uint8_t enc_button_interrupt_pin = 4; // SCK/PB1 is intterupt 4 on ItsyBitsy
const uint8_t enc_button_pin_phys = SCK; //encoder button pin - ItsyBitsy pin

const uint8_t enc_button_ground = MISO; //MISO 12, can also delete this and just use ground
const uint8_t enc_ground = 2; // Encoder ground, can also delte this and just use ground

// Globals to store states for the loop
uint8_t last_state = 0; // A is bit position 0, B is position 1

// Flags used for tracking the movement of the encoder
// A more in depth description of each below in the loop
uint8_t enc_flags = 0;

// Flag used to avoid muting every cycle the button is down
char button_pressed = 0;

void setup() {
  // Starting USB
  Consumer.begin();

  // Setting pins as input and to be high by default, the encoder can only
  // pull pins low or leave them floating
  pinMode(enc_pin_A_phys, INPUT_PULLUP);
  pinMode(enc_pin_B_phys, INPUT_PULLUP);
  pinMode(enc_button_pin_phys, INPUT_PULLUP);
  pinMode(enc_ground, OUTPUT);
  pinMode(enc_button_ground, OUTPUT);

  attachInterrupt(enc_button_interrupt_pin, handle_button_click, CHANGE); //pin 7
  attachInterrupt(enc_pin_A, handle_rotary_enc_change, CHANGE); //pin 1
  attachInterrupt(enc_pin_B, handle_rotary_enc_change, CHANGE); //pin 3

  digitalWrite(enc_ground, LOW);
  digitalWrite(enc_button_ground, LOW);

  // Initial reading of encoder states
  if(bit_is_set(enc_PORT_REG, enc_pin_A))
    last_state |= (1 << 0);
  if(bit_is_set(enc_PORT_REG, enc_pin_B))
    last_state |= (1 << 1);
}

void loop(){
  
}

void handle_rotary_enc_change() {

  // Variable to keep track of the encoder direction
  // -1 is CCW, 0 is no movement and 1 is CW
  int8_t dir = 0;

  // Encoder pins current states
  uint8_t state = 0; // A is bit position 0, B is bit position 1
  if(bit_is_set(enc_PORT_REG, enc_pin_A)){
    state |= (1 << 0);
  }
  if(bit_is_set(enc_PORT_REG, enc_pin_B)){
    state |= (1 << 1);
  }

  /*
   * Encoder flags descriptions and encoder workings:
   * 
   * This code is based off of the quadrature output table on the datasheet, 
   * looking there will greatly help understand the workings of the following code.
   * 
   * The encoder in its resting state will not pull either pins low. AB = HIGH HIGH
   * Only transitions from the resting state are counted to simplify the counting.
   * 
   * Bit positions for an 8-bit byte (uint8_t):
   * 76543210
   * 
   * Flags guide:
   * Flag | From AB   | To AB     | Bit position
   * -----|-----------|-----------|-------------
   * A_CW | HIGH HIGH | LOW  HIGH | 0
   * A_CCW| LOW HIGH  | HIGH HIGH | 1
   * B_CW | HIGH LOW  | HIGH HIGH | 2
   * B_CCW| HIGH HIGH | HIGH LOW  | 3
   * 
   * middle is when the ecoder is pulling both pins low and is in
   * the direct middle of a transition - bit position 4
   * 
   */

  // If there is any change in the encoder pins
  if (last_state != state) {

    // If both pins were previously high (resting state)
    if (last_state == B00000011) {
      if (state == B00000010) {
        enc_flags |= (1 << 0); // A_CW AB HIGH HIGH to LOW HIGH
      }
      else if (state == B00000001) {
       enc_flags |= (1 << 3); // B_CCW AB HIGH HIGH to HIGH LOW
      }
    }

    // If both pins are low
    if (state == B00000000) {
      enc_flags |= (1 << 4); // The ecoder is in the middle of a transision
    }
    // If the encoder is a rest state, inbetween clicks, pulling neither pin low
    else if (state == B00000011) {
      if (last_state == B00000001) {
        enc_flags |= (1 << 2); // B_CW AB HIGH LOW to HIGH HIGH
      }
      else if (last_state == B00000010) {
        enc_flags |= (1 << 1); // A_CCW AB LOW HIGH to HIGH HIGH
      }

      // Now that the knob is in a resting position, rotation will be checked
        // If both clockwise edges were hit then the encoder rotated clockwise
        //  the middle edge is added in to debounce and stop false movements
      if (enc_flags == B00010101) { // A_CW && B_CW && middle
        dir = 1;
      }
      // If both counter-clockwise edges were hit then the encoder rotated counter-clockwise
      // the middle edge is added in to debounce and stop false movements
      else if (enc_flags == B00011010) { // A_CCW && B_CCW && middle
        dir = -1;
      }

      // Reseting the flags, only when the knob is in a resting position
      enc_flags = 0;
    }
  }

  last_state = state;

  // Clockwise movement
  if (dir > 0) {
    Consumer.write(MEDIA_VOLUME_UP);
  }
  // Counter-clockwise movement
  else if (dir < 0) {
    Consumer.write(MEDIA_VOLUME_DOWN);
  }
}


void handle_button_click() {
    // Check if the button is pushed (pulling the pin low)
  if (bit_is_clear(button_PORT_REG, enc_button_pin)) {
    // Check is the button was previously not pressed
    if (button_pressed == 0) {
      Consumer.write(MEDIA_VOLUME_MUTE);
      
      //Debounce
      delay(5);
    }
    button_pressed = 1;
  }
  else {
    if (button_pressed) {
      // Debounce
      delay(5);
    }
    button_pressed = 0;
  }
}

